mod object_allocation_pool;
use object_allocation_pool::ObjectAllocationPool;
use object_allocation_pool::ObjectAllocation;

#[derive(Debug)]
pub enum AllocationError {
    NotEnoughSpace,
    ZeroSize
}

#[derive(Debug)]
pub struct AllocationPool {
    size: usize,
    unallocated: Vec<AllocationBlock>
}

impl AllocationPool {
    pub fn new(size: usize) -> Result<Self, AllocationError> {
        if size == 0 {
            return Err(AllocationError::ZeroSize);
        }

        Ok(Self {
            size,
            unallocated: vec![AllocationBlock { offset: 0, end: size - 1 }]
        })
    }

    pub fn get_block(&mut self, size: usize) -> Result<AllocationBlock, AllocationError> {
        if size == 0 {
            return Err(AllocationError::ZeroSize);
        }

        for i in 0..self.unallocated.len() {
            if self.unallocated[i].len() >= size {
                let unallocated = self.unallocated.swap_remove(i);
                let block = AllocationBlock {
                    offset: unallocated.offset,
                    end: unallocated.offset + size - 1
                };

                if unallocated.end > block.end {
                    self.unallocated.push(AllocationBlock {
                        offset: block.end + 1,
                        end: unallocated.end
                    })
                }

                return Ok(block);
            }
        }

        Err(AllocationError::NotEnoughSpace)
    }

    pub fn free_block(&mut self, block: AllocationBlock) {
        let mut i = 0;
        let mut new_block = block;

        while i < self.unallocated.len() {
            let comp = self.unallocated[i];

            if new_block.overlaps_with(&comp) {
                let new_offset = comp.offset;
                let new_end = if comp.end > new_block.end { comp.end } else { new_block.end };

                new_block.offset = new_offset;
                new_block.end = new_end;

                self.unallocated.swap_remove(i);

                i = 0;
            } else {
                i += 1;
            }
        }

        self.unallocated.push(new_block);
    }
}

#[derive(Debug, Copy, Clone)]
pub struct AllocationBlock {
    offset: usize,
    end: usize
}

impl AllocationBlock {
    pub fn len(&self) -> usize {
        self.end - self.offset + 1
    }

    pub fn offset(&self) -> usize {
        self.offset
    }

    fn overlaps_with(&self, other: &Self) -> bool {
        (self.offset < other.offset && (self.end + 1) >= other.offset)
            || (other.offset < self.offset && (other.end + 1) >= self.offset)
    }
}

impl PartialEq for AllocationBlock {
    fn eq(&self, rhs: &Self) -> bool {
        self.offset == rhs.offset && self.end == rhs.end
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_block_size_makes_sense() {
        assert_eq!(AllocationBlock {offset: 0, end: 0}.len(), 1);
        assert_eq!(AllocationBlock {offset: 0, end: 1}.len(), 2);
        assert_eq!(AllocationBlock {offset: 1, end: 2}.len(), 2);
        assert_eq!(AllocationBlock {offset: 0, end: 2}.len(), 3);
    }

    #[test]
    fn test_new_allocation_block_state_is_correct() {
        let pool = AllocationPool::new(2).unwrap();

        assert_eq!(pool.size, 2);
        assert_eq!(pool.unallocated.len(), 1);
        assert_eq!(pool.unallocated[0], AllocationBlock { offset: 0, end: 1 });
    }

    #[test]
    fn test_overlapping_blocks() {
        assert!(!AllocationBlock {offset: 0, end: 2}.overlaps_with(
            &AllocationBlock {offset: 6, end: 7}
        ));

        assert!(!AllocationBlock {offset: 6, end: 7}.overlaps_with(
            &AllocationBlock {offset: 0, end: 2}
        ));

        assert!(AllocationBlock {offset: 0, end: 2}.overlaps_with(
            &AllocationBlock {offset: 1, end: 1}
        ));

        assert!(AllocationBlock {offset: 1, end: 1}.overlaps_with(
            &AllocationBlock {offset: 0, end: 2}
        ));

        assert!(AllocationBlock {offset: 0, end: 2}.overlaps_with(
            &AllocationBlock {offset: 1, end: 3}
        ));

        assert!(AllocationBlock {offset: 1, end: 3}.overlaps_with(
            &AllocationBlock {offset: 0, end: 2}
        ));

        assert!(AllocationBlock {offset: 0, end: 1}.overlaps_with(
            &AllocationBlock {offset: 1, end: 2}
        ));

        assert!(AllocationBlock {offset: 1, end: 2}.overlaps_with(
            &AllocationBlock {offset: 0, end: 1}
        ));

        assert!(AllocationBlock {offset: 8, end: 13}.overlaps_with(
            &AllocationBlock {offset: 2, end: 7}
        ));

        assert!(AllocationBlock {offset: 2, end: 7}.overlaps_with(
            &AllocationBlock {offset: 8, end: 13}
        ));
    }

    #[test]
    fn test_valid_get_block() {
        let mut pool = AllocationPool::new(2).unwrap();
        let result = pool.get_block(2);

        assert!(result.is_ok());

        let block = result.unwrap();

        assert_eq!(block.offset, 0);
        assert_eq!(block.len(), 2);

        assert_eq!(pool.unallocated.len(), 0);
    }

    #[test]
    fn test_invalid_get_block() {
        let mut pool = AllocationPool::new(1).unwrap();
        let result = pool.get_block(2);

        assert!(result.is_err());

        assert_eq!(pool.unallocated.len(), 1);
        assert_eq!(pool.unallocated[0], AllocationBlock { offset: 0, end: 0 });
    }

    #[test]
    fn test_invalid_second_get_block() {
        let mut allocation = AllocationPool::new(2).unwrap();
        let first_result = allocation.get_block(2);
        let second_result = allocation.get_block(2);

        assert!(first_result.is_ok());
        assert!(second_result.is_err());
    }

    #[test]
    fn test_valid_second_get_block() {
        let mut pool = AllocationPool::new(4).unwrap();
        let first_result = pool.get_block(2);
        let second_result = pool.get_block(2);

        assert!(first_result.is_ok());
        assert!(second_result.is_ok());

        let block = second_result.unwrap();

        assert_eq!(block.offset, 2);
        assert_eq!(block.len(), 2);

        assert_eq!(pool.unallocated.len(), 0);
    }

    #[test]
    fn test_block_frees_correctly() {
        let mut pool = AllocationPool::new(2).unwrap();

        let first_block = pool.get_block(2).unwrap();
        pool.free_block(first_block);

        let second_result = pool.get_block(2);

        assert!(second_result.is_ok());

        let block = second_result.unwrap();

        assert_eq!(block.offset, 0);
        assert_eq!(block.len(), 2);
    }

    #[test]
    fn test_a_lot_of_operations() {
        let mut pool = AllocationPool::new(20).unwrap();
        let _a = pool.get_block(2).unwrap();
        let b1 = pool.get_block(6).unwrap();
        let _b2 = pool.get_block(6).unwrap();
        let _b3 = pool.get_block(6).unwrap();

        assert!(pool.get_block(1).is_err());
        assert_eq!(pool.unallocated.len(), 0);

        pool.free_block(b1);
        assert_eq!(pool.unallocated.len(), 1);
        assert_eq!(pool.unallocated[0], AllocationBlock { offset: 2, end: 7 });

        let _c1 = pool.get_block(1).unwrap();
        let _c2 = pool.get_block(2).unwrap();
        let _c3 = pool.get_block(3).unwrap();

        assert!(pool.get_block(1).is_err());
    }

    #[test]
    fn test_a_lot_more_operations() {
        let mut pool = AllocationPool::new(100).unwrap();
        let _a = pool.get_block(2).unwrap();
        let b1 = pool.get_block(6).unwrap();
        let b2 = pool.get_block(6).unwrap();
        let _b3 = pool.get_block(6).unwrap();
        let _b4 = pool.get_block(6).unwrap();
        let b5 = pool.get_block(8).unwrap();
        let _b6 = pool.get_block(4).unwrap();


        assert_eq!(pool.unallocated.len(), 1);
        assert_eq!(pool.unallocated[0], AllocationBlock { offset: 38, end: 99 });

        pool.free_block(b1);

        assert_eq!(pool.unallocated.len(), 2);
        assert_eq!(pool.unallocated[0], AllocationBlock { offset: 38, end: 99 });
        assert_eq!(pool.unallocated[1], AllocationBlock { offset: 2, end: 7 });

        pool.free_block(b5);

        assert_eq!(pool.unallocated.len(), 3);
        assert_eq!(pool.unallocated[0], AllocationBlock { offset: 38, end: 99 });
        assert_eq!(pool.unallocated[1], AllocationBlock { offset: 2, end: 7 });
        assert_eq!(pool.unallocated[2], AllocationBlock { offset: 26, end: 33 });

        pool.free_block(b2);

        assert_eq!(pool.unallocated.len(), 3);
        assert_eq!(pool.unallocated[0], AllocationBlock { offset: 38, end: 99 });
        assert_eq!(pool.unallocated[1], AllocationBlock { offset: 26, end: 33 });
        assert_eq!(pool.unallocated[2], AllocationBlock { offset: 2, end: 13 });
    }

    #[test]
    fn test_allocating_zero_fails() {
        let mut pool = AllocationPool::new(20).unwrap();

        assert!(pool.get_block(0).is_err());
    }

    #[test]
    fn test_making_pool_size_zero_fails() {
        assert!(AllocationPool::new(0).is_err());
    }
}
