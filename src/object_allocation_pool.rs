use std::mem::ManuallyDrop;
use std::ops::Deref;
use std::ops::DerefMut;
use std::sync::Arc;
use std::sync::RwLock;

#[derive(Debug)]
pub struct ObjectAllocationPool<T> {
    available_objects: Arc<RwLock<Vec<T>>>
}

impl<T> Clone for ObjectAllocationPool<T> {
    fn clone(&self) -> Self {
        Self {
            available_objects: self.available_objects.clone()
        }
    }
}

impl<T> ObjectAllocationPool<T> {
    pub fn new(initial: Vec<T>) -> Self {
        Self {
            available_objects: Arc::new(RwLock::new(initial))
        }
    }

    pub fn add_item(&self, item: T) {
        self.available_objects.write().unwrap().push(item);
    }

    pub fn allocate(&self) -> Option<ObjectAllocation<T>> {
        match self.available_objects.write().unwrap().pop() {
            Some(item) => Some(self.make_allocation(item)),
            None => None
        }
    }

    pub fn make_allocation(&self, item: T) -> ObjectAllocation<T> {
        ObjectAllocation::new(item, self.clone())
    }
}

#[derive(Debug)]
pub struct ObjectAllocation<T> {
    pool: ObjectAllocationPool<T>,
    item: ManuallyDrop<T>
}

impl<T: PartialEq> PartialEq for ObjectAllocation<T> {
    fn eq(&self, rhs: &Self) -> bool {
        self.item == rhs.item
    }
}

impl<T> ObjectAllocation<T> {
    fn new(item: T, pool: ObjectAllocationPool<T>) -> Self {
        Self {
            pool: pool,
            item: ManuallyDrop::new(item)
        }
    }
}

impl<T> Deref for ObjectAllocation<T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        self.item.deref()
    }
}

impl<T> DerefMut for ObjectAllocation<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.item.deref_mut()
    }
}

impl<T> Drop for ObjectAllocation<T> {
    fn drop(&mut self) {
        self.pool.add_item(unsafe { ManuallyDrop::<T>::take(&mut self.item) });
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_allocation() {
        let pool : ObjectAllocationPool<u8> = ObjectAllocationPool::new(vec![]);

        assert_eq!(pool.make_allocation(1), pool.make_allocation(1));
        assert!(pool.make_allocation(2) != pool.make_allocation(1));
    }

    #[test]
    fn test_empty_pool_allocated_none() {
        let pool : ObjectAllocationPool<u8> = ObjectAllocationPool::new(vec![]);

        assert_eq!(None, pool.allocate());
    }

    #[test]
    fn test_allocates_from_initial_vec() {
        let pool : ObjectAllocationPool<u8> = ObjectAllocationPool::new(vec![2]);

        let allocation = pool.allocate();
        let cmp = Some(pool.make_allocation(2));

        assert_eq!(cmp, allocation);
        assert_eq!(None, pool.allocate());
    }

    #[test]
    fn test_drop_deallocates() {
        let pool : ObjectAllocationPool<u8> = ObjectAllocationPool::new(vec![]);

        let allocation = pool.make_allocation(5);
        drop(allocation);

        let cmp = Some(pool.make_allocation(5));
        assert_eq!(cmp, pool.allocate());
    }

    #[test]
    fn test_add_item() {
        let pool : ObjectAllocationPool<u8> = ObjectAllocationPool::new(vec![]);

        pool.add_item(5);

        let cmp = Some(pool.make_allocation(5));
        assert_eq!(cmp, pool.allocate());
    }
}
